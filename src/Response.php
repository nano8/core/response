<?php

namespace laylatichy\nano\core;

use JetBrains\PhpStorm\Pure;
use JsonException;
use Workerman\Protocols\Http\Response as WorkermanResponse;

final class Response {
    public function __construct(
        private HttpCode $code = HttpCode::OK,
        private string $body = '',
    ) {
    }

    public function code(HttpCode $code): void {
        $this->code = $code;
    }

    public function plain(string $body): void {
        $this->body = $body;
    }

    public function json(array $body): void {
        try {
            $this->body = (string)json_encode(value: $body, flags: JSON_THROW_ON_ERROR | JSON_UNESCAPED_SLASHES);
        } catch (JsonException) {
            throw new NanoException(message: 'invalid json');
        }
    }

    #[Pure]
    public function getCode(): HttpCode {
        return $this->code;
    }

    #[Pure]
    public function getBody(): string {
        return $this->body;
    }

    public function notFound(array $headers = []): WorkermanResponse {
        $this->code(code: HttpCode::NOT_FOUND);

        $this->json(
            body: [
                'code'     => $this->getCode()->code(),
                'response' => $this->getCode()->name,
            ]
        );

        return $this->send($this->getCode(), $headers, $this->getBody());
    }

    #[Pure]
    public function send(HttpCode $code = HttpCode::OK, array $headers = [], string $body = ''): WorkermanResponse {
        return new WorkermanResponse(
            status: $code->code(),
            headers: $headers,
            body: $body
        );
    }
}

