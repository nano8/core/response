#!/usr/bin/env node

import { $ } from 'zx';

import 'zx/globals';

$.verbose = true;

(async () => {
    const flags = [
        '-lsa',
    ];

    await $`ls ${flags}`;
})();
