### nano/core/response

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-response?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-response)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-response?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-response)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/response/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/response/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/response/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/response/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/response/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/response/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-response`
